<?php

namespace Framework\Controller;

use App\Config\Configuration;
use App\Entity\User;
use Framework\Templating\View;
use Framework\Http\Routing;

abstract class AbstractController
{
    public $templateName;

    public function __construct()
    {
        $class = get_called_class();
        $parts = explode("\\", $class);
        $lastElement = end($parts);
        $parts = explode("Controller", $lastElement);

        $rootDir = Configuration::getRootDir();
        $this->templateName = $rootDir.'App/View/'.$parts[0].'/layout.tpl.php';
    }

    public function render($file, $vars = array())
    {
        $objView = new View();

        return $objView->renderView($this->templateName, $file, $vars);
    }

    public function redirectTo($routeName, $routeParams = array())
    {
        $redirectUrl = Routing::generateUrl($routeName, $routeParams);

        header('Location: '.$redirectUrl);
    }

    /**
     * @return User
     */
    public function getUser()
    {
        $sessionId = session_id();
        $user = User::findOneBy(array('session_id' => $sessionId));

        return $user;
    }
}
