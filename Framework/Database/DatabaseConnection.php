<?php

namespace Framework\Database;

use App\Config\DB;

class DatabaseConnection
{
    /**
     * @var \PDO $_instance
     */
    private static $_instance = null;

    private function __construct() {}

    private function __clone() {}

    private function __sleep() {}

    private function __wakeup() {}

    public static function getInstance()
    {
        $const = DB::databaseParams();
        if (self::$_instance === null)
        {
            try
            {
                self::$_instance = new \PDO($const['DB_TYPE'].':host='.$const['DB_HOST'].';dbname='.$const['DB_NAME'], $const['DB_USER'], $const['DB_PASS'], array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
                self::$_instance->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            }
            catch (\PDOException $e)
            {
                throw new \Exception('DB Error');
            }
        }

        return self::$_instance;
    }

    final public function __destruct()
    {
        self::$_instance = null;
    }

}