CREATE TABLE IF NOT EXISTS `ws_user` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `session_id` varchar(255) NOT NULL,
	  `first_name` varchar(255) NOT NULL,
	  `last_name` varchar(255) NOT NULL,
	  `phone` varchar(255) NOT NULL,
	   PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
	
CREATE TABLE IF NOT EXISTS `ws_category` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `name` varchar(255) NOT NULL,
	  `description` text,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
	
CREATE TABLE IF NOT EXISTS `ws_good` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `category_id` int(11) NOT NULL,
	  `name` varchar(255) NOT NULL,
	  `description` text,
	  `price` varchar(255) NOT NULL,
	  `picture` varchar(255) NOT NULL,
	   PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
	
CREATE TABLE IF NOT EXISTS `ws_cart` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `goods_id` int(11) NOT NULL,
	  `user_id` int(11) NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
	
CREATE TABLE IF NOT EXISTS `ws_order` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `user_id` int(11) NOT NULL,
	  `description` text,
	  `phone` varchar(255) NOT NULL,
	  `delivery_type` varchar(255) NOT NULL,
	  `total_sum` varchar(255) NOT NULL,
	   PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
	
CREATE TABLE IF NOT EXISTS `ws_order_details` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `order_id` int(11) NOT NULL,
	  `goods_id` int(11) NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
	