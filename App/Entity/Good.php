<?php

namespace App\Entity;

use Framework\Database\DatabaseConnection;
use Framework\Database\Repository;
/**
 * Created by PhpStorm.
 * User: NotePad
 * Date: 18.08.2015
 * Time: 15:44
 */
class Good extends Repository
{
    public static $staticTableName = 'ws_good';

    private $id;

    private $categoryId;

    private $name;

    private $description;

    private $price;

    private $picture;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param mixed $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    public static function getTotalCountOfCategory($id)
    {
        $db = DatabaseConnection::getInstance();

        $result = $db->query('SELECT COUNT(*) AS t_count FROM ' . self::$staticTableName.' WHERE category_id = '.$id, \PDO::FETCH_ASSOC);

        return $result->fetchAll();
    }

}
