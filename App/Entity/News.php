<?php

namespace App\Entity;

use Framework\Database\DatabaseConnection;
use Framework\Database\Repository;

class News extends Repository
{
    public static $staticTableName = 'ws_news';

    private $id;

    private $header;

    private $picture;

    private $shortNew;

    private $author;

    private $createdAt;

    private $fullNew;

    public static function getTotalCountOfNews()
    {
        $db = DatabaseConnection::getInstance();

        $result = $db->query('SELECT COUNT(*) AS t_count FROM ' . self::$staticTableName, \PDO::FETCH_ASSOC);

        return $result->fetchAll();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param mixed $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getShortNew()
    {
        return $this->shortNew;
    }

    /**
     * @param mixed $shortNew
     */
    public function setShortNew($shortNew)
    {
        $this->shortNew = $shortNew;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getFullNew()
    {
        return $this->fullNew;
    }

    /**
     * @param mixed $fullNew
     */
    public function setFullNew($fullNew)
    {
        $this->fullNew = $fullNew;
    }


}

