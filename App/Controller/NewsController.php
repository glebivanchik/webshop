<?php

namespace App\Controller;

use App\Config\Configuration;
use App\Entity\News;
use Framework\Controller\AbstractController;
use Framework\Http\Request;

class NewsController extends AbstractController
{

    public function indexAction(Request $request)
    {
        $rootDir = Configuration::getRootDir();
        $this->templateName = $rootDir.'App/View/Shop/layout.tpl.php';

        $newsOnPage = 3;
        $params = $request->getGetParameters();
        $totalCount = News::getTotalCountOfNews();
        $numPages = ceil($totalCount[0]['t_count']/$newsOnPage);
        $currentPage = $params['page'] ?: 1;
        $offset = ($currentPage - 1)*$newsOnPage;
        $news = News::findBy(array('limit' => $newsOnPage, 'offset' => $offset));

        return $this->render('News/news.tpl.php', array('news' => $news, 'numPages' => $numPages, 'currentPage' => $currentPage));
    }

    public function newsItemAction(Request $request)
    {
        $rootDir = Configuration::getRootDir();
        $this->templateName = $rootDir.'App/View/Shop/layout.tpl.php';

        $params = $request->getGetParameters();

        $newsItem = News::findOneBy(array('id' => $params['id']));

        return $this->render('News/new_item.tpl.php', array('newsItem' => $newsItem));
    }


}