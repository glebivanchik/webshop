<?php

namespace App\Controller;

use App\Config\Configuration;
use App\Entity\Cart;
use App\Entity\Category;
use App\Entity\Good;
use App\Entity\Order;
use App\Entity\OrderDetails;
use App\Entity\User;
use Framework\Controller\AbstractController;
use Framework\Http\Request;

class ShopController extends AbstractController
{
	public function indexAction(Request $request)
	{
		$goods = Good::findBy(array('limit' => 6, 'order_by_asc' => 'RAND()'));

		return $this->render('Shop/index.tpl.php', array('goods' => $goods));
	}

	public function cartAction(Request $request)
	{
		$this->processOrder($request);
		$params = $request->getGetParameters();

		$rootDir = Configuration::getRootDir();
		$this->templateName = $rootDir.'App/View/Shop/layout_not_left_menu.tpl.php';
		$goods = array();
		$user = $this->getUser();

		$goodsCart = array();
		if (is_object($user)) {
			$goodsCart = Cart::findBy(array('user_id' => $user->getId()));
		}

		if (count($goodsCart)) {
			foreach ($goodsCart as $key => $value) {
				if (isset($goods[$value->getGoodsId()])) {
					$goods[$value->getGoodsId()]['count']++;
					continue;
				}
				$goods[$value->getGoodsId()] = array('good' => Good::findOneBy(array('id' => $value->getGoodsId())), 'count' => 1, 'price' => substr(Good::findOneBy(array('id' => $value->getGoodsId()))->getPrice(), 1));
			}
		}

		return $this->render('Shop/cart.tpl.php', array('goods' => $goods, 'success' => $params['success'], 'error' => $params['error']));
	}


	public function categoryItemAction(Request $request)
	{
		$params = $request->getGetParameters();
		$totalCount = Good::getTotalCountOfCategory($params['id']);
		$numPages = ceil($totalCount[0]['t_count']/9);
		$currentPage = $params['page'] ?: 1;
		$offset = ($currentPage - 1)*9;
		$goods = Good::findBy(array('category_id' => $params['id'], 'limit' => 9, 'offset' => $offset));

		return $this->render('Shop/category.tpl.php', array('goods' => $goods, 'numPages' => $numPages, 'currentPage' => $currentPage, 'category_id' => $params['id']));
	}

	public function goodAction(Request $request)
	{
		$params = $request->getGetParameters();
		$good = Good::findBy(array('id' => $params['id']));
		$category = Category::findBy(array('id' => $good[0]->getCategoryId(), 'limit' => 1));
		$randomGoods = Good::findBy(array('category_id' => $good[0]->getCategoryId(), 'limit' => 3, 'order_by_asc' => 'RAND()'));

		return $this->render('Shop/good.tpl.php', array('good' => $good, 'randomGoods' => $randomGoods, 'category' => $category[0]->getName()));
	}

	public function ajaxAddToCartAction(Request $request)
	{
		$getParams = $request->getGetParameters();
		$postParams = $request->getPostParameters();
		$user = $this->getUser();
		if (!is_object($user)) {
			$sessionId = session_id();
			$user = new User();
			$user->setSessionId($sessionId);
			$user->setFirstName('');
			$user->setLastName('');
			$user->setPhone('');
			$user->save();
		}
		if (isset($postParams['count']) && $postParams['count']) {
			for ($i=1; $i <= $postParams['count']; $i++) {
				$cart = new Cart();
				$cart->setGoodsId($getParams['id']);
				$cart->setUserId($user->getId());
				$cart->save();

			}
		} else {
			$cart = new Cart();
			$cart->setGoodsId($getParams['id']);
			$cart->setUserId($user->getId());
			$cart->save();

		}

		$result = json_encode(array('status' => 1));
		echo $result;
	}

	public function ajaxCartProcessingAction(Request $request)
	{
		$postParams = $request->getPostParameters();
		$user = $this->getUser();
		$errorMsg = null;
		switch ($postParams['operation']) {
			case '+':
				$cart = new Cart();
				$cart->setGoodsId($postParams['goodId']);
				$cart->setUserId($user->getId());
				$cart->save();
				break;
			case '-':
				$cart = Cart::findOneBy(array('user_id' => $user->getId(), 'goods_id' => $postParams['goodId']));
				if (is_object($cart)) {
					$cart->remove();
				} else {
					$errorMsg = 'No goods';
				}
				break;
			case 'del':
				$cart = Cart::findBy(array('user_id' => $user->getId(), 'goods_id' => $postParams['goodId']));
				if (count($cart)) {
					foreach ($cart as $v) {
						$v->remove();
					}
				} else {
					$errorMsg = 'No goods';
				}
				break;
		}
		if ($errorMsg) {
			$result = json_encode(array('status' => 0, 'message' => $errorMsg));
		} else {
			$result = json_encode(array('status' => 1, 'operation' => $postParams['operation']));
		}
		echo $result;
	}

	private function processOrder(Request $request)
	{
		if ($request->isPost()) {
			$postParams = $request->getPostParameters();
			$user = $this->getUser();
			$goodsCart = Cart::findBy(array('user_id' => $user->getId()));
			$order = new Order();
			$order->setUserId($user->getId());
			$order->setDescription($postParams['description']);
			$order->setPhone($postParams['phone']);
			$order->setDeliveryType($postParams['delivery_type']);
			$order->setTotalSum($postParams['total_sum']);
			$order->save();
			if (count($goodsCart)) {
				foreach ($goodsCart as $k => $val) {
					$orderDetails = new OrderDetails();
					$orderDetails->setOrderId($order->getId());
					$orderDetails->setGoodsId($val->getGoodsId());
					$orderDetails->save();
					$val->remove();
				}
			} else {
				$this->redirectTo('shop.cart', array('error' => 1));
				return false;
			}
			$this->redirectTo('shop.cart', array('success' => 1));
			return false;
		}
	}

	public function ordersAction(Request $request)
	{
		$rootDir = Configuration::getRootDir();
		$this->templateName = $rootDir.'App/View/Shop/layout_not_left_menu.tpl.php';

        $orders = array();
		$user = $this->getUser();
		if (is_object($user)) {
            /** @var Order[] $orders */
            $orders = Order::findBy(array('user_id' => $user->getId()));
        }

		$result = array();
		if (count($orders)) {
			foreach ($orders as $k => $order) {
				/** @var OrderDetails[] $orderDetails */
				$orderDetails = OrderDetails::findBy(array('order_id' => $order->getId()));
				$goods = array();
				if (count($orderDetails)) {
					foreach ($orderDetails as $good) {

						$goods[] = Good::findOneBy(array('id' => $good->getGoodsId()));
					}
					$result[] = array('order' => $order, 'goods' => $goods);
				}
			}
		}
		return $this->render('Shop/orders.tpl.php', array('orders' => $result));
	}
}
