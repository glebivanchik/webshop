<?php

namespace App\Config;

class Configuration
{

    public static function routeTable()
    {
        $ar = array(
            'shop.index' => array(
                '/',
                'shop',
                'index',
            ),
            'shop.cart' => array(
                '/cart',
                'shop',
                'cart',
            ),
            'news.index' => array(
                '/news',
                'news',
                'index',
            ),
            'news.item' => array(
                '/news_item/{id}',
                'news',
                'newsItem',
            ),
            'shop.orders' => array(
                '/orders',
                'shop',
                'orders',
            ),
            'shop.category_item' => array(
                '/category/{id}',
                'shop',
                'categoryItem',
            ),
            'shop.good' => array(
                '/good/{id}',
                'shop',
                'good',
            ),
            'shop.ajax.add_to_cart' => array(
                '/ajax/add-to-cart/{id}',
                'shop',
                'ajaxAddToCart',
            ),
            'shop.ajax.cart_processing' => array(
                '/ajax/cart_processing',
                'shop',
                'ajaxCartProcessing',
            ),
            'feedback.index' => array(
                '/feedback',
                'feedback',
                'index',
            ),
        );

        return $ar;
    }

    public static function getRootDir()
    {
        return __DIR__ . '/../../';
    }

    public static function getNotFoundTemplateNamePath()
    {
        return self::getRootDir().'App/View/404.php';
    }
}
