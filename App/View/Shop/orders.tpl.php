<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">My Orders</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <table data-url="<?= Framework\Templating\ViewHelper::url('shop.ajax.cart_processing'); ?>"
                   class="table table-condensed">
                <thead>
                <tr class="cart_menu">
                    <th class="image">Description Order</th>
                    <th class="image">Phone</th>
                    <th class="image">Delivery Type</th>
                    <th class="quantity">Goods</th>
                    <th class="total">Total</th>
                </tr>
                </thead>
                <tbody>
        <?php if (count($orders)) {
            foreach ($orders as $key => $order) {?>
        <tr class="good_item" data-id="">
            <td class="v-center h-center">
                <?= $order['order']->getDescription(); ?>
            </td>
            <td class=" v-center h-center">
                <?= $order['order']->getPhone(); ?>
            </td>
            <td class="v-center h-center">
                <?= $order['order']->getDeliveryType(); ?>
            </td>
            <td class="">
                <?php if (count($order['goods'])) {
                foreach ($order['goods'] as $key1 => $good) {?>
                    <div class="cart-goods">
                        <span class=""><img src="<?= $good->getPicture(); ?>" width="50" alt=""></span>
                        <span class=""><a class="" href="<?= Framework\Templating\ViewHelper::url('shop.good', array('id' => $good->getId())); ?>"><?= $good->getName(); ?></a></span>
                       <span class=""><?= $good->getPrice(); ?></span>
                    </div>
                <?php }
                } ?>
            </td>
            <td class="cart_total">
                <p class="cart_total_price">$ <?= $order['order']->getTotalSum(); ?></p>
            </td>
        </tr>
         <?php }
            } ?>
</tbody>
</table>
</div>
</div>
</section> <!--/#cart_items-->


