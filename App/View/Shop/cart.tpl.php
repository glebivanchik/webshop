<section id="cart_items" class="cart_page">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Shopping Cart</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <table data-url="<?= Framework\Templating\ViewHelper::url('shop.ajax.cart_processing'); ?>"
                   class="table table-condensed">
                <thead>
                <tr class="cart_menu">
                    <th class="image">Item</th>
                    <th class="description"></th>
                    <th class="price">Price</th>
                    <th class="quantity">Quantity</th>
                    <th class="total">Total</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php if (count($goods)) {
                    foreach ($goods as $key => $value) { ?>
                        <tr class="good_item" data-id="<?= $value['good']->getId(); ?>">
                            <td class="cart_product">
                                <a href="#"><img src="<?= $value['good']->getPicture(); ?>" width="150" alt=""></a>
                            </td><td class="cart_description">
                                <h4><a href="#"><?= $value['good']->getName(); ?></a></h4>

                                <p>Web ID: <?= $value['good']->getId(); ?></p>
                            </td><td class="cart_price">
                                <p class="prise_item"><?= $value['good']->getPrice(); ?></p>
                            </td><td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <a class="cart_quantity_down" href="#"> - </a>
                                    <input class="cart_quantity_input" type="text" name="quantity"
                                           value="<?= $value['count']; ?>" autocomplete="off" size="2" >
                                    <a class="cart_quantity_up" href="#"> + </a>
                                </div>
                            </td><td class="cart_total">
                                <p class="cart_total_price">$ <?= $value['price'] * $value['count']; ?></p>
                            </td><td class="cart_delete">
                                <a class="cart_quantity_delete" href="#"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>
            <? if (!count($goods)): ?>
                <div>
                    Your cart is empty.
                </div>

            <? endif; ?>
        </div>
    </div>
</section> <!--/#cart_items-->

<?php if (count($goods)): ?>
    <section id="do_action">
        <div class="container">
            <div class="heading">
                <h3>What would you like to do next?</h3>

                <p>Choose if you have a discount code or reward points you want to use or would like to estimate your
                    delivery cost.</p>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="total_area">
                        <ul>
                            <li>Cart Sub Total <span class="total_price_all_goods">$</span></li>
                            <li>Eco Tax <span class="eco_tax">$2</span></li>
                            <li>Shipping Cost <span>Free</span></li>
                            <li>Total <span class="total_price_tax_free">$</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="chose_area">
                        <ul>
                            <form id='order_form' class="user_info" action="" method="post">
                                <p><b>Description:</b></p>

                                <p><textarea class="description1" rows="10" cols="45" name="description"></textarea></p>

                                <p class="zip-field"><b>Delivery Type:</b><br>
                                    <input class="delivery_type" type="text" size="40" name="delivery_type"/>
                                </p>

                                <p class="zip-field"><b>Phone:</b><br>
                                    <input class="phone" type="text" size="40" name="phone"/>
                                </p>
                                <input type="hidden" name="total_sum" id="total_sum"/>
                                <a class="btn btn-default update pull-right" id='submit_order' href="">Submit Order</a>
                            </form>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#do_action-->
<?php endif; ?>
<script>
    <?php if (isset($success) && $success == 1) { ?>
    $(function () {
        $('#modal4').modal('show');
    });
    <?php  } ?>
    <?php if (isset($error) && $error == 1) { ?>
    $(function () {
        $('#modal5').modal('show');
    });
    <?php  } ?>
</script>