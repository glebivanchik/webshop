<?php $addToCartUrl =  Framework\Templating\ViewHelper::url('shop.ajax.add_to_cart', array('id' => $value->getId())); ?>
<div class='col-sm-4'>
    <div class='product-image-wrapper'>
        <div class='single-products'>
            <div class='productinfo text-center'>
                <img src='<?= $value->getPicture(); ?>' alt='' />
                <h2><?= $value->getPrice(); ?></h2>
                <a href='<?= $url; ?>'><?= $value->getName();?></a><br>
                <a href='<?= $addToCartUrl; ?>' class='btn btn-default add-to-cart'"><i class='fa fa-shopping-cart'></i>Add to cart</a>
            </div>
            <div class='product-overlay'>
                <div class='overlay-content'>
                    <h2><?= $value->getPrice(); ?></h2>
                    <a href='<?= $url; ?>' class='link'><?= $value->getName(); ?></a><br>
                    <a href='<?= $addToCartUrl; ?>' class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Add to cart</a>
                </div>
            </div>
        </div>
    </div>
</div>