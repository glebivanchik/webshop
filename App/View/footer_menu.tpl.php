<?php $urlIndex = Framework\Templating\ViewHelper::url('shop.index');
$urlCart = Framework\Templating\ViewHelper::url('shop.cart');
$urlNews = Framework\Templating\ViewHelper::url('news.index');
$urlFeedback = Framework\Templating\ViewHelper::url('feedback.index');
$urlOrders = Framework\Templating\ViewHelper::url('shop.orders'); ?>
<ul class="nav nav-pills nav-stacked">
    <li><a href="<?= $urlIndex; ?>" class="active">Home</a></li>
    <li><a href="<?= $urlIndex; ?>#categories" class="active">Categories</a></li>
    <li><a href="<?= $urlCart; ?>" class="active">Cart</a></li>
    <li><a href="<?= $urlNews; ?>" class="active">News</a></li>
    <li><a href="<?= $urlFeedback; ?>" class="active">Feedback</a></li>
    <li><a href="<?= $urlOrders ?>" class="active">My Orders</a></li>
</ul>