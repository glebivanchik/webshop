<?php
$urlIndex = Framework\Templating\ViewHelper::url('shop.index');
$urlCart = Framework\Templating\ViewHelper::url('shop.cart');
$urlNews = Framework\Templating\ViewHelper::url('news.index');
$urlFeedback = Framework\Templating\ViewHelper::url('feedback.index');
$urlOrders = Framework\Templating\ViewHelper::url('shop.orders');
$urlActive = App\Templating\ViewHelper::getCurrentUrl();
$isNewItem = App\Templating\ViewHelper::isNewItem();
?>
<ul class="nav navbar-nav collapse navbar-collapse">
    <li><a href="<?= $urlIndex; ?>" class="<?= ($urlIndex == $urlActive) ? 'active' : ''; ?>" >Home</a></li>
    <li><a href="<?= $urlIndex; ?>#categories" class="">Categories</a></li>
    <li><a href="<?= $urlCart; ?>" class="<?= ($urlCart == $urlActive) ? 'active' : ''; ?>" >Cart</a></li>
    <li><a href="<?= $urlNews; ?>" class="<?= (($urlNews == $urlActive) || $isNewItem) ? 'active' : ''; ?>" >News</a></li>
    <li><a href="<?= $urlFeedback; ?>" class="<?= ($urlFeedback == $urlActive) ? 'active' : ''; ?>" >Feedback</a></li>
    <li><a href="<?= $urlOrders; ?>" class="<?= ($urlOrders == $urlActive) ? 'active' : ''; ?>" >My Orders</a></li>
</ul>